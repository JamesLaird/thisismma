# Angular Web Client

This is an Angular (v9) web application that provides a user interface to 

- list all stock details 

- view stock audit history

- search stock audit history by product

- download the current stock details and audit history as a JSON file

## Prerequisites

This Angular project has dependencies that require Node 10.9 or higher

The nvm ls and nvm use {nodeVersionNumber} commands can help configure your machine to point to the correct Node version

- Valid Internet Browser

The current Angular v9 browser default support is for so-called "evergreen" browsers; the last versions of browsers that automatically update themselves. This includes Safari >= 10, Chrome >= 55 (including Opera), Edge >= 13 on the desktop, and iOS 10 and Chrome on mobile.

## Project Setup

This project was created via the Angular CLI in order to take advantage of its many developer friendly and performance tuning features. 
Please install this application via npm if you intend to run this code locally or create a production ready build.

- https://angular.io/guide/setup-local
- https://github.com/angular/angular-cli
- https://angular.io/docs/ts/latest/cli-quickstart.html

You can verify that the Angular CLI was successfully installed by running this command `ng -v` which will display the current Angular CLI version.

## Project Build

After the Angular CLI has been installed successfully, from a command prompt or terminal window go to the project web-client folder, then type the following cmd `npm install` to build the required node_modules:
	
## Local Development Server

From a command prompt or terminal window go to the project web-client folder and type `ng serve` to launch a local a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Production Build

From a command prompt or terminal window go to the project client folder and type `ng build --prod` to build the project for a production ready release. The build artifacts will be stored in the `dist/` directory which can then be deployed to the server of your choice.  

Using the `--prod` flag will take advantage of the Angular CLI the Ahead-of-Time (AOT) Compiler feature:
https://angular.io/guide/aot-compiler

The `--prod` flag will also make use of different optimisation techniques including bundling, tree-shaking, and run dead code elimination via UglifyJS.

## Running Unit Tests

From a command prompt or terminal window go to the project client folder and type `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Code Quality Check

From a command prompt or terminal window go to the project client folder and type `ng lint`. This will highlight any linting errors that require resolved.

