import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {catchError} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ThisIsMmaService {

  private LOCAL_ROOT = '//localhost:8080';
  //private API_ROOT = '//thisismma-env.eba-gvxcr5y2.us-east-1.elasticbeanstalk.com';
  private ADD_LINK_API = this.LOCAL_ROOT + '/addLink';
  private UPDATE_LINK_API = this.LOCAL_ROOT + '/updateLink';
  private GET_LINKS_BY_CATEGORY_API = this.LOCAL_ROOT + '/getLinksByCategory';
  private GET_LINKS_BY_COMPANY_API = this.LOCAL_ROOT + '/getLinksByCompany';
  private GET_ENTITY_API = this.LOCAL_ROOT + '/getEntity';
  private CATEGORY_PARAM = '?category=';
  private COMPANY_PARAM = '?company=';
  private ID_PARAM = '?id=';

  constructor(private http: HttpClient) {
  }

  getAll(endpoint: string): Observable<any> {
    return this.http.get(this.LOCAL_ROOT + endpoint).pipe(
      catchError(this.handleError(this.LOCAL_ROOT + endpoint, []))
    );
  }

  getLinksByCategory(category: string): Observable<any> {
    return this.http.get(this.GET_LINKS_BY_CATEGORY_API + this.CATEGORY_PARAM + category).pipe(
      catchError(this.handleError(this.GET_LINKS_BY_CATEGORY_API, []))
    );
  }

  getLinksByCompany(company: string): Observable<any> {
    return this.http.get(this.GET_LINKS_BY_COMPANY_API + this.COMPANY_PARAM + company).pipe(
      catchError(this.handleError(this.GET_LINKS_BY_COMPANY_API, []))
    );
  }

  getLinkById(id: string): Observable<any> {
    return this.http.get(this.GET_ENTITY_API + this.ID_PARAM + id).pipe(
      catchError(this.handleError(this.GET_ENTITY_API, []))
    );
  }

  addLink(link: any): Observable<any> {
    //link.linkUrl = 'https://www.youtube.com/embed/' + link.linkUrl;
    const result = this.http.post(this.ADD_LINK_API, link);
    return result;
  }

  updateLink(link: any): Observable<any> {
    const result = this.http.put(this.UPDATE_LINK_API, link);
    return result;
  }

  handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error); // log to console
      return of(result as T);
    };
  }
}
