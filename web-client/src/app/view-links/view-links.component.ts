import {Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {ThisIsMmaService} from '../shared/this-is-mma.service';
import {MatTableDataSource} from '@angular/material/table';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-view-links',
  templateUrl: './view-links.component.html',
  styleUrls: ['./view-links.component.css']
})
export class ViewLinksComponent implements OnInit {

  displayedColumns: string[] = [ 'imageLink', 'linkName', 'linkId', 'videoUrl', 'id'];

  dataSource: any;

  category: any;

  categories: any;

  company: string;

  categoriesEndpoint = '/getCategories';

  embededBlocked = '/getBlockedLinks';

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

  constructor(private thisIsMMAService: ThisIsMmaService, private router: Router, private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.thisIsMMAService.getAll(this.categoriesEndpoint).subscribe(data => {
      this.categories = data;
    });

    this.route.queryParams.subscribe(params => {
      const { category } = params;

      if (category !== null && category !== '') {
        this.category = category;
        this.getLinksByCategory();
      }
    });



    // this.getLinksByCompany();
  }

  getLinksByCategory() {
    this.thisIsMMAService.getLinksByCategory(this.category).subscribe(data => {
      this.dataSource = new MatTableDataSource<LinkElement>(data);
      this.dataSource.paginator = this.paginator;
    });
  }

  getLinksByCompany() {
    this.thisIsMMAService.getLinksByCompany(this.company).subscribe(data => {
      this.dataSource = new MatTableDataSource<LinkElement>(data);
      this.dataSource.paginator = this.paginator;
    });
  }

  getBlockedLinks() {
    this.thisIsMMAService.getAll(this.embededBlocked).subscribe(data => {
      this.dataSource = new MatTableDataSource<LinkElement>(data);
      this.dataSource.paginator = this.paginator;
    });
  }

  editLink(id) {
    this.router.navigate([`edit-link`],
      { queryParams: { id, category: this.category} });
  }
}

export interface LinkElement {
  id: number;
  linkName: string;
  linkUrl: string;
  imageLink: string;
  videoUrl: string;
}
