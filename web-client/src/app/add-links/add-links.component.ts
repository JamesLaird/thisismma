import { Component, OnInit } from '@angular/core';
import {ThisIsMmaService} from '../shared/this-is-mma.service';
import {FlashMessagesService} from 'angular2-flash-messages';
import {NgForm} from '@angular/forms';

@Component({
  selector: 'app-add-links',
  templateUrl: './add-links.component.html',
  styleUrls: ['./add-links.component.css']
})
export class AddLinksComponent implements OnInit {

  constructor(private thisIsMMAService: ThisIsMmaService,
              private flashMessagesService: FlashMessagesService) {
  }

   link: any = {};

    ngOnInit(): void {
    }


  handleError(error) {
    const errorMessage = error;
    this.flashMessagesService.show(`${errorMessage.error.error}`, {cssClass: 'alert-danger', timeout: 5000});
  }

  save(form: NgForm) {
    this.thisIsMMAService.addLink(form).subscribe(result => {
      // tslint:disable-next-line:max-line-length
      this.flashMessagesService.show(`Link ${this.link.linkName} has been successfully added!`, {cssClass: 'alert-success', timeout: 5000});
      this.link = {};
    }, error => this.handleError(error));
  }


}
