import {Component, OnInit} from '@angular/core';
import {ThisIsMmaService} from '../shared/this-is-mma.service';
import {FlashMessagesService} from 'angular2-flash-messages';
import {NgForm} from '@angular/forms';

@Component({
  selector: 'app-add-playlist',
  templateUrl: './add-playlist.component.html',
  styleUrls: ['./add-playlist.component.css']
})
export class AddPlaylistComponent implements OnInit {

  constructor(private thisIsMMAService: ThisIsMmaService,
              private flashMessagesService: FlashMessagesService) {
  }

  link1: any = {};
  link2: any = {};
  link3: any = {};
  link4: any = {};
  link5: any = {};
  link6: any = {};
  link7: any = {};
  link8: any = {};
  link9: any = {};
  link10: any = {};

  linkCategory: string;
  playlistA: string;


  ngOnInit(): void {
    /*this.link1.linkName = 'Facebook';
    this.link2.linkName = 'Instagram';
    this.link3.linkName = 'Website';
    this.link4.linkName = 'YouTube';
    this.link5.linkName = 'Twitter';
    this.linkCategory = 'LINK_MMA_CLUBS';*/
  }


  handleError(error) {
    const errorMessage = error;
    this.flashMessagesService.show(`${errorMessage.error.error}`, {cssClass: 'alert-danger', timeout: 5000});
  }

  save(form: NgForm) {

    if (this.link1.linkName !== null && this.link1.linkName !== '') {
      this.addPlaylist(this.link1);
    }

    if (this.link2.linkName !== null && this.link2.linkName !== '') {
      this.addPlaylist(this.link2);
    }

    if (this.link3.linkName !== null && this.link3.linkName !== '') {
      this.addPlaylist(this.link3);
    }

    if (this.link4.linkName !== null && this.link4.linkName !== '') {
      this.addPlaylist(this.link4);
    }

    if (this.link5.linkName !== null && this.link5.linkName !== '') {
      this.addPlaylist(this.link5);
    }

    if (this.link6.linkName !== null && this.link6.linkName !== '') {
      this.addPlaylist(this.link6);
    }

    if (this.link7.linkName !== null && this.link7.linkName !== '') {
      this.addPlaylist(this.link7);
    }

    if (this.link8.linkName !== null && this.link8.linkName !== '') {
      this.addPlaylist(this.link8);
    }

    if (this.link9.linkName !== null && this.link9.linkName !== '') {
      this.addPlaylist(this.link9);
    }

    if (this.link10.linkName !== null && this.link10.linkName !== '') {
      this.addPlaylist(this.link10);
    }



  }

  addPlaylist(link) {
    link.linkCategory = this.linkCategory;
    link.playlistA = this.playlistA;
    link.linkUrl = 'https://www.youtube.com/embed/' + link.linkUrl;
    this.thisIsMMAService.addLink(link).subscribe(result => {
      // tslint:disable-next-line:max-line-length
      this.flashMessagesService.show(`Link ${link.linkName} has been successfully added!`, {cssClass: 'alert-success', timeout: 5000});
    }, error => this.handleError(error));
  }

}
