import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {NgForm} from '@angular/forms';
import {ThisIsMmaService} from '../shared/this-is-mma.service';
import {FlashMessagesService} from 'angular2-flash-messages';

@Component({
  selector: 'app-edit-link',
  templateUrl: './edit-link.component.html',
  styleUrls: ['./edit-link.component.css']
})
export class EditLinkComponent implements OnInit {

  constructor(private route: ActivatedRoute,
              private thisIsMMAService: ThisIsMmaService,
              private flashMessagesService: FlashMessagesService) {
  }

  link: any;
  category: string;

  ngOnInit(): void {
    this.route.queryParams.subscribe(params => {
      const {id, category } = params;
      this.category = category;
      this.getLink(id);
    });
  }

  getLink(id: string) {
    this.thisIsMMAService.getLinkById(id).subscribe(data => {
      this.link = data;
    });
  }

  handleError(error) {
    const errorMessage = error;
    this.flashMessagesService.show(`${errorMessage.error.error}`, {cssClass: 'alert-danger', timeout: 5000});
  }

  save(form: NgForm) {
    this.thisIsMMAService.updateLink(form).subscribe(result => {
      // tslint:disable-next-line:max-line-length
      this.flashMessagesService.show(`Link ${this.link.linkName} has been successfully updated!`, {cssClass: 'alert-success', timeout: 5000});
      form.reset();
    }, error => this.handleError(error));
  }

}
