import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

import {ViewLinksComponent} from './view-links/view-links.component';
import {EditLinkComponent} from './edit-link/edit-link.component';
import {AddLinksComponent} from './add-links/add-links.component';
import {AddPlaylistComponent} from './add-playlist/add-playlist.component';
import {TestComponent} from './test/test.component';

const routes: Routes = [
  {path: '', redirectTo: '/test', pathMatch: 'full'},
  {
    path: 'view-links',
    component: ViewLinksComponent
  },
  {
    path: 'edit-link',
    component: EditLinkComponent
  },
  {
    path: 'add-links',
    component: AddLinksComponent
  },
  {
    path: 'add-playlist',
    component: AddPlaylistComponent
  },
  {
    path: 'test',
    component: TestComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
