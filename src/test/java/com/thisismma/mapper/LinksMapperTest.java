package com.thisismma.mapper;

import org.junit.Test;

import static org.junit.Assert.*;

public class LinksMapperTest {

    @Test
    public void mapImageLink() throws Exception {
        String link = LinksMapper.mapImageLink("https://player.vimeo.com/video/122663858",  "test" );
        System.out.println(link);
        assertNotNull(link);
    }

    @Test
    public void mapImageLinkYoutube() throws Exception {
        String link = LinksMapper.mapImageLink("https://www.youtube.com/channel/UCj2wWX8p4_EeCbMCquxQojg",  "SBG Ireland YouTube Channel" );
        System.out.println(link);
        assertNull(link);
    }

}
