package com.thisismma.json;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class VideosRequest {

    private List<Videos> highlightVideos;
    private List<Videos> documentaryVideos;
    private List<Videos> playlistVideos;
    private List<Videos> fightersVideos;
    private List<Videos> mmaTeamsVideos;
    private List<Videos> tvSeriesVideos;
    private List<Videos> filmProducersVideos;

}
