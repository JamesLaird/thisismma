package com.thisismma.json;

import com.thisismma.jpa.VideosEntity;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class VideosResponse {

    private List<VideosEntity> highlightVideos;
    private List<VideosEntity> documentaryVideos;
    private List<VideosEntity> playlistVideos;
    private List<VideosEntity> fightersVideos;
    private List<VideosEntity> mmaTeamsVideos;
    private List<VideosEntity> tvSeriesVideos;
    private List<VideosEntity> filmProducersVideos;

}
