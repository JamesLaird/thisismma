package com.thisismma.json;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Videos {

    private String id;
    private String title;
    private String displayTitle;
    private String image;
    private String filterby;
    private String category;
    private String categoryB;
    private String description;
    private String type;
    private String type2;
    private String icon;
    private Boolean isTabs;
    private Boolean isTeams;
    private Boolean showAds;
}
