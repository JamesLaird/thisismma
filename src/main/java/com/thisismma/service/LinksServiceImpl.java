package com.thisismma.service;

import com.thisismma.jpa.LinksEntity;
import com.thisismma.jpa.LinksRepository;
import com.thisismma.mapper.CategoriesMapper;
import com.thisismma.mapper.LinksMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import static com.thisismma.mapper.LinksMapper.validLink;
import static java.util.stream.Collectors.toList;

@Service
public class LinksServiceImpl implements LinksService {

    @Autowired
    private LinksRepository repository;

    @Autowired
    ResourceLoader resourceLoader;

    @Override
    public Map<String, List> getAllLinks() {
        return LinksMapper.mapMMALinks(repository.findAll());
    }

    @Override
    public List<LinksEntity> getLinksByCategory(String category) {
        List<LinksEntity> links = repository.findByLinkCategory(category);

        if (links.isEmpty()) {
            links = repository.findByPlaylistA(category);
            links.addAll(repository.findByPlaylistB(category));
            links = links.stream().filter(link -> !link.getEmbedBlocked()).collect(toList());
        } else {
            links = links.stream().filter(link -> validLink(link, category)).collect(toList());
        }

        LinksMapper.orderLinks(links);
        return links;
    }

    @Override
    public List<LinksEntity> getBlockedLinks() {
        return repository.findByEmbedBlocked(true);
    }

    @Override
    public void add(LinksEntity link) {
        repository.save(link);
    }

    @Override
    public void update(LinksEntity link) {
        repository.save(link);
    }

    @Override
    public List<LinksEntity> getVideosOfTheDay() {
        List<LinksEntity> links = repository.findAll();
        LinksMapper.orderLinks(links);
        return links.stream().filter(link -> isVideoOfTheDay(link)).collect(toList());
    }

    @Override
    public Map<String, List> getBoxingLinks() {
        return LinksMapper.mapBoxingLinks(repository.findAll());
    }

    private boolean isVideoOfTheDay(LinksEntity link) {
        return link.getIncludeAsVideoOfDay() && !link.getEmbedBlocked();
    }

    @Override
    public Optional<LinksEntity> getEntity(Long id) {
        return repository.findById(id);
    }

    @Override
    public List<String> getCategories() {
        return CategoriesMapper.getCategories();
    }

    public List<LinksEntity> getLinksByCompany(String company) {
        List<LinksEntity> links = repository.findAll();
        LinksMapper.orderLinks(links);
        return links.stream().filter(link -> link.getLinkUrl().indexOf(company) >= 0).collect(toList());
    }

    @Override
    public List<LinksEntity> getLinksByUrl(String linkUrl) {
        return repository.findByLinkUrl(linkUrl);
    }
}
