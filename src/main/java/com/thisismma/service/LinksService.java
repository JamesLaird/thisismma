package com.thisismma.service;

import com.thisismma.jpa.LinksEntity;
import com.thisismma.json.VideosResponse;
import org.json.JSONException;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public interface LinksService {

    Map<String, List> getAllLinks();

    List<LinksEntity> getLinksByCategory(String category);

    List<LinksEntity> getBlockedLinks();

    List<String> getCategories();

    List<LinksEntity> getLinksByCompany(String company);

    List<LinksEntity> getLinksByUrl(String linkUrl);

    Optional<LinksEntity> getEntity(Long id);

    void add(LinksEntity link);

    void update(LinksEntity link);

    List<LinksEntity> getVideosOfTheDay();

    Map<String, List> getBoxingLinks();
}
