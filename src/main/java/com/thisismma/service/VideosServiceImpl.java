package com.thisismma.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.thisismma.enums.mma.VideoTypesEnum;
import com.thisismma.jpa.VideosEntity;
import com.thisismma.jpa.VideosRepository;
import com.thisismma.json.Videos;
import com.thisismma.json.VideosRequest;
import com.thisismma.json.VideosResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.util.List;

import static java.util.stream.Collectors.toList;

@Service
public class VideosServiceImpl implements VideosService {

    @Autowired
    private VideosRepository repository;

    @Override
    public VideosResponse getVideos() {

        VideosResponse videosResponse = new VideosResponse();

        List<VideosEntity> links = repository.findAll();

        videosResponse.setHighlightVideos(links.stream().filter(link -> link.getPlaylistType().equals(VideoTypesEnum.HIGHLIGHT_VIDEOS.toString())).collect(toList()));

        videosResponse.setDocumentaryVideos(links.stream().filter(link -> link.getPlaylistType().equals(VideoTypesEnum.DOCUMENTARY_VIDEOS.toString())).collect(toList()));

        videosResponse.setFightersVideos(links.stream().filter(link -> link.getPlaylistType().equals(VideoTypesEnum.FIGHTERS_VIDEOS.toString())).collect(toList()));

        videosResponse.setMmaTeamsVideos(links.stream().filter(link -> link.getPlaylistType().equals(VideoTypesEnum.MMA_TEAMS_VIDEOS.toString())).collect(toList()));

        videosResponse.setPlaylistVideos(links.stream().filter(link -> link.getPlaylistType().equals(VideoTypesEnum.PLAYLIST_VIDEOS.toString())).collect(toList()));

        videosResponse.setFilmProducersVideos(links.stream().filter(link -> link.getPlaylistType().equals(VideoTypesEnum.FILM_PRODUCER_VIDEOS.toString())).collect(toList()));

        videosResponse.setTvSeriesVideos(links.stream().filter(link -> link.getPlaylistType().equals(VideoTypesEnum.TV_SERIES_VIDEOS.toString())).collect(toList()));

        return videosResponse;
    }

    @Override
    public void add() throws IOException {
        String filename = "VideoPlaylists.json";
        ObjectMapper objectMapper = new ObjectMapper();
        File file = new File(
                this.getClass().getClassLoader().getResource(filename).getFile()
        );
        VideosRequest videosRequest = objectMapper.readValue(file, VideosRequest.class);

        videosRequest.getHighlightVideos()
                .forEach(video -> repository.save(mapVideo(video, VideoTypesEnum.HIGHLIGHT_VIDEOS.toString())));

        videosRequest.getDocumentaryVideos()
                .forEach(video -> repository.save(mapVideo(video, VideoTypesEnum.DOCUMENTARY_VIDEOS.toString())));

        videosRequest.getFightersVideos()
                .forEach(video -> repository.save(mapVideo(video, VideoTypesEnum.FIGHTERS_VIDEOS.toString())));

        videosRequest.getMmaTeamsVideos()
                .forEach(video -> repository.save(mapVideo(video, VideoTypesEnum.MMA_TEAMS_VIDEOS.toString())));

        videosRequest.getPlaylistVideos()
                .forEach(video -> repository.save(mapVideo(video, VideoTypesEnum.PLAYLIST_VIDEOS.toString())));

        videosRequest.getFilmProducersVideos()
                .forEach(video -> repository.save(mapVideo(video, VideoTypesEnum.FILM_PRODUCER_VIDEOS.toString())));

        videosRequest.getTvSeriesVideos()
                .forEach(video -> repository.save(mapVideo(video, VideoTypesEnum.TV_SERIES_VIDEOS.toString())));


    }

    private VideosEntity mapVideo(Videos videoJson, String playlistType) {

        VideosEntity entity = new VideosEntity();
        entity.setTitle(videoJson.getTitle());
        entity.setDisplayTitle(videoJson.getDisplayTitle());
        entity.setPlaylistType(playlistType);
        entity.setImage(videoJson.getImage());
        entity.setCategoryOne(videoJson.getCategory());
        entity.setCategoryTwo(videoJson.getCategoryB());
        entity.setDescription(videoJson.getDescription());
        entity.setType(videoJson.getType());
        entity.setIsTabs(videoJson.getIsTabs() != null && Boolean.valueOf(videoJson.getIsTabs()) ? videoJson.getIsTabs() : false);
        entity.setIsTeams(videoJson.getIsTeams() != null && Boolean.valueOf(videoJson.getIsTeams()) ? videoJson.getIsTeams() : false);
        entity.setShowAds(videoJson.getShowAds() != null && Boolean.valueOf(videoJson.getShowAds()) ? videoJson.getShowAds() : false);
        return entity;

    }
}
