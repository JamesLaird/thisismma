package com.thisismma.service;

import com.thisismma.jpa.FeedbackEntity;
import com.thisismma.jpa.FeedbackRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class FeedbackServiceImpl implements FeedbackService {

    @Autowired
    private FeedbackRepository repository;

    @Override
    public List<FeedbackEntity> getAllFeedback() {
        return repository.findAll();
    }

    @Override
    public Optional<FeedbackEntity> getEntity(Long id) {
        return Optional.empty();
    }

    @Override
    public void add(FeedbackEntity feedback) {
        repository.save(feedback);
    }
}
