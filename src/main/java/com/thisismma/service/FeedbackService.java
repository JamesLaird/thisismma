package com.thisismma.service;

import com.thisismma.jpa.FeedbackEntity;

import java.util.List;
import java.util.Optional;

public interface FeedbackService {
    List<FeedbackEntity> getAllFeedback();

    Optional<FeedbackEntity> getEntity(Long id);

    void add(FeedbackEntity feedback);
}
