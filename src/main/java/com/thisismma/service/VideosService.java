package com.thisismma.service;

import com.thisismma.json.VideosResponse;
import org.json.JSONException;

import java.io.IOException;

public interface VideosService {

    VideosResponse getVideos() throws JSONException, IOException;

    void add() throws IOException;
}
