package com.thisismma;

import com.thisismma.jpa.LinksRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;

import java.util.List;

@EnableCaching // Add this
@SpringBootApplication
public class ThisIsMMAApplication {

	private static final Logger logger = LoggerFactory.getLogger(ThisIsMMAApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(ThisIsMMAApplication.class, args);
	}

	@Bean
	ApplicationRunner init(LinksRepository repository) {
		return args -> {
			//logger.info("Starting loading links");

			//List links = repository.findAll();


			//logger.info("Finished loading links " + links.size());
		};
	}

}
