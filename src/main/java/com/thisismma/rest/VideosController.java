package com.thisismma.rest;

import com.thisismma.json.VideosResponse;
import com.thisismma.service.VideosServiceImpl;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class VideosController {

    @Autowired
    private
    VideosServiceImpl videosService;

    @Autowired
    CacheManager cacheManager;

    @GetMapping("/getAllVideos")
    @Cacheable(value = "videosCache")
    public VideosResponse getAllVideos() throws IOException, JSONException {
        return videosService.getVideos();
    }

    @GetMapping("/addVideos")
    public ResponseEntity<?> addVideos() throws IOException {
        videosService.add();
        return new ResponseEntity<String>(HttpStatus.CREATED);
    }

}
