package com.thisismma.rest;

import com.thisismma.error.LinkErrorResponse;
import com.thisismma.jpa.LinksEntity;
import com.thisismma.json.VideosResponse;
import com.thisismma.service.LinksServiceImpl;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class LinksController {

    @Autowired
    private
    LinksServiceImpl linksService;

    @Autowired
    CacheManager cacheManager;

    @GetMapping("/getAllLinks")
    @Cacheable(value = "linksCache")
    public Map<String, List> getAllLinks() {
        return linksService.getAllLinks();
    }

    @GetMapping("/getBoxingLinks")
    public Map<String, List> getBoxingLinks() {
        return linksService.getBoxingLinks();
    }

    @GetMapping("/refreshCache")
    public void evictAllCaches() {
        cacheManager.getCacheNames().stream()
                .forEach(cacheName -> cacheManager.getCache(cacheName).clear());
    }

    @GetMapping("/getVideoOfTheDay")
    public LinksEntity getVideosOfTheDay() {
        Calendar calendar = Calendar.getInstance();
        int dayOfYear = calendar.get(Calendar.DAY_OF_YEAR);
        return linksService.getVideosOfTheDay().get(dayOfYear);
    }

    @GetMapping("/getLinksByCategory")
    public List<LinksEntity> getLinksByCategory(@RequestParam final String category) {
        return linksService.getLinksByCategory(category);
    }

    @GetMapping("/getLinksByCompany")
    public List<LinksEntity> getLinksByCompany(@RequestParam final String company) {
        return linksService.getLinksByCompany(company);
    }

    @GetMapping("/getBlockedLinks")
    public List<LinksEntity> getBlockedLinks() {
        return linksService.getBlockedLinks();
    }

    @GetMapping("/getCategories")
    public List<String> getCategories() {
        return linksService.getCategories();
    }

    @GetMapping("/getEntity")
    public Optional<LinksEntity> getEntity(@RequestParam final Long id) {
        return linksService.getEntity(id);
    }

    @PostMapping("/addLink")
    public ResponseEntity<?> addLink(@RequestBody LinksEntity link) {

        List<LinksEntity> links = linksService.getLinksByUrl(link.getLinkUrl());

        for (LinksEntity obj : links) {
            if (StringUtils.equals(link.getPlaylistA(), obj.getPlaylistA()) || StringUtils.equals(link.getPlaylistB(), obj.getPlaylistB())) {
                return new ResponseEntity(new LinkErrorResponse("Unable to create. A link with that url " +
                        link.getLinkUrl() + " already exists."), HttpStatus.CONFLICT);
            }
        }

        if (link.getIncludeInMainSection() == null) {
            link.setIncludeInMainSection(false);
        }
        if (link.getIncludeAsVideoOfDay() == null) {
            link.setIncludeAsVideoOfDay(false);
        }

        link.setDateAdded(new Date());
        link.setEmbedBlocked(false);
        link.setDeletedFlag(false);
        linksService.add(link);
        return new ResponseEntity<String>(HttpStatus.CREATED);
    }

    @PutMapping("/updateLink")
    public ResponseEntity<?> updateLink(@RequestBody LinksEntity link) {
        linksService.update(link);
        return new ResponseEntity<String>(HttpStatus.OK);
    }
}
