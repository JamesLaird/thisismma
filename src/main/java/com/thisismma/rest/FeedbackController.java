package com.thisismma.rest;
import com.thisismma.jpa.FeedbackEntity;
import com.thisismma.service.FeedbackServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@CrossOrigin
public class FeedbackController {

    @Autowired
    private
    FeedbackServiceImpl feedbackService;

    @GetMapping("/getAllFeedback")
    public List<FeedbackEntity> getAllFeedback() {
        return feedbackService.getAllFeedback();
    }

    @PostMapping("/addFeedback")
    public ResponseEntity<?> addFeedback(@RequestBody FeedbackEntity feedback) {
        feedback.setDateAdded(new Date());
        feedbackService.add(feedback);
        return new ResponseEntity<String>(HttpStatus.CREATED);
    }

}
