package com.thisismma.mapper;

import com.thisismma.enums.boxing.BoxersEnum;
import com.thisismma.enums.boxing.BoxingTrainingEnum;
import com.thisismma.enums.mma.*;

import java.util.ArrayList;
import java.util.List;

public class CategoriesMapper {

    public static List<String> getCategories() {
        
        List<String> categories = new ArrayList<>();

        for (DocumentaryEnum doc : DocumentaryEnum.values()) {
            categories.add(doc.toString());
        }

        for (MartialArtsEnum martialArts : MartialArtsEnum.values()) {
            categories.add( martialArts.toString());
        }

        for (MMATeamsEnum teams : MMATeamsEnum.values()) {
            categories.add(teams.toString());
        }

        for (PlaylistEnum playlist : PlaylistEnum.values()) {
            categories.add(playlist.toString());
        }

        for (TVSeriesEnum tvSeries : TVSeriesEnum.values()) {
            categories.add(tvSeries.toString());
        }

        for (FightersEnum fighters : FightersEnum.values()) {
            categories.add(fighters.toString());
        }

        for (FilmProducersEnum filmProducers : FilmProducersEnum.values()) {
            categories.add(filmProducers.toString());
        }

        for (LinksEnum links : LinksEnum.values()) {
            categories.add(links.toString());
        }

        for (BoxersEnum boxers : BoxersEnum.values()) {
            categories.add(boxers.toString());
        }

        for (BoxingTrainingEnum boxingCategories : BoxingTrainingEnum.values()) {
            categories.add(boxingCategories.toString());
        }


        /*
        for (ArticlesEnum articles : ArticlesEnum.values()) {
            categories.add(articles.toString());
        }*/

        return categories;

    }
}
