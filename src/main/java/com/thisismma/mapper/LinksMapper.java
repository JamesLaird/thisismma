package com.thisismma.mapper;

import com.thisismma.enums.boxing.BoxersEnum;
import com.thisismma.enums.boxing.BoxingCategoriesEnum;
import com.thisismma.enums.boxing.BoxingPlaylistsEnum;
import com.thisismma.enums.boxing.BoxingTrainingEnum;
import com.thisismma.enums.mma.*;
import com.thisismma.jpa.LinksEntity;
import com.thisismma.jpa.LinksEntityResponse;
import org.apache.commons.lang3.EnumUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.util.stream.Collectors.toList;

public class LinksMapper {

    private static Map<String, List> linksMap = new HashMap<>();

    private static Map<String, List> boxingLinksMap = new HashMap<>();

    public static Map<String, List> mapMMALinks(List<LinksEntity> links) {

        for (DocumentaryEnum doc : DocumentaryEnum.values()) {
            populateLinks(links, doc.toString(), false);
        }

        for (MartialArtsEnum martialArts : MartialArtsEnum.values()) {
            populateLinks(links, martialArts.toString(), false);
        }

/*
        for (ArticlesEnum articles : ArticlesEnum.values()) {
            populateLinks(links, articles.toString());
        }

        for (LinksEnum linksEnum : LinksEnum.values()) {
            populateLinks(links, linksEnum.toString());
        }
*/

        for (MMATeamsEnum teams : MMATeamsEnum.values()) {
            populatePlaylist(links, teams.toString(), false);
        }

        for (PlaylistEnum playlist : PlaylistEnum.values()) {
            populatePlaylist(links, playlist.toString(), false);
        }

        for (TVSeriesEnum tvSeries : TVSeriesEnum.values()) {
            populatePlaylist(links, tvSeries.toString(), false);
        }

        for (FightersEnum fighters : FightersEnum.values()) {
            populatePlaylist(links, fighters.toString(), false);
        }

        for (FilmProducersEnum filmProducers : FilmProducersEnum.values()) {
            populatePlaylist(links, filmProducers.toString(), false);
        }

        /*for (BoxersEnum boxers : BoxersEnum.values()) {
            populatePlaylist(links, boxers.toString());
        }

        for (BoxingTrainingEnum boxingCategories : BoxingTrainingEnum.values()) {
            populatePlaylist(links, boxingCategories.toString());
        }*/

        populateWhatsNewlist(links);


        return linksMap;

    }

    public static Map<String, List> mapBoxingLinks(List<LinksEntity> links) {

        for (BoxingCategoriesEnum doc : BoxingCategoriesEnum.values()) {
            populateLinks(links, doc.toString(), true);
        }

        for (BoxingPlaylistsEnum playlist : BoxingPlaylistsEnum.values()) {
            populatePlaylist(links, playlist.toString(), true);
        }

        for (BoxersEnum boxers : BoxersEnum.values()) {
            populatePlaylist(links, boxers.toString(), true);
        }

        for (BoxingTrainingEnum boxingCategories : BoxingTrainingEnum.values()) {
            populatePlaylist(links, boxingCategories.toString(), true);
        }

        populateWhatsNewBoxinglist(links);

        populateBoredFilmBoxinglist(links);


        return boxingLinksMap;

    }

    private static void populateLinks(List<LinksEntity> links, String category, boolean isBoxing) {
        List list = links.stream().filter(link -> validLink(link, category)).collect(toList());
        orderLinks(list);


        List<LinksEntityResponse> newList = new ArrayList<>(list.size());
        list.forEach(e -> newList.add(mapObject((LinksEntity) e)));


        if (isBoxing) {
            boxingLinksMap.put(category, newList);
        } else {
            linksMap.put(category, newList);
        }
    }


    private static LinksEntityResponse mapObject(LinksEntity entity) {
        return new LinksEntityResponse(entity.getLinkName(), entity.getLinkUrl(), entity.getImageLink(), entity.getDoc());
    }

    public static boolean validLink(LinksEntity entity, String linkCategory) {
        return (StringUtils.equals(linkCategory, entity.getLinkCategory()) && !entity.getEmbedBlocked() && includeInMainLists(entity));
    }

    private static boolean includeInMainLists(LinksEntity entity) {
        return entity.getIncludeInMainSection() || (StringUtils.isEmpty(entity.getPlaylistA()) && StringUtils.isEmpty(entity.getPlaylistB()));
    }


    private static boolean validPlaylist(String playlistCategory, String category, boolean isEmbedBlocked) {
        return StringUtils.equals(playlistCategory, category) && !isEmbedBlocked;
    }

    private static void populatePlaylist(List<LinksEntity> links, String category, boolean isBoxing) {
        List list = links.stream().filter(link -> validPlaylist(link.getPlaylistA(), category, link.getEmbedBlocked()) || validPlaylist(link.getPlaylistB(), category, link.getEmbedBlocked())).collect(toList());
        orderLinks(list);
        List<LinksEntityResponse> newList = new ArrayList<>(list.size());
        list.forEach(e -> newList.add(mapObject((LinksEntity) e)));

        if (isBoxing) {
            boxingLinksMap.put(category, newList);
        } else {
            linksMap.put(category, newList);
        }

    }

    private static void populateWhatsNewlist(List<LinksEntity> links) {
        List list = links.stream().filter(link -> isDateWithinLast30Days(link) && isMMALink(link)).collect(toList());
        orderLinks(list);
        List<LinksEntityResponse> newList = new ArrayList<>(list.size());
        list.forEach(e -> newList.add(mapObject((LinksEntity) e)));


        linksMap.put("WHATS_NEW", newList);
    }

    private static void populateWhatsNewBoxinglist(List<LinksEntity> links) {
        List list = links.stream().filter(link -> isDateWithinLast30Days(link) && isBoxingLink(link)).collect(toList());
        orderLinks(list);
        List<LinksEntityResponse> newList = new ArrayList<>(list.size());
        list.forEach(e -> newList.add(mapObject((LinksEntity) e)));


        boxingLinksMap.put("WHATS_NEW", newList);
    }

    private static void populateBoredFilmBoxinglist(List<LinksEntity> links) {
        List list = links.stream().filter(link -> StringUtils.equals(link.getPlaylistA(), TVSeriesEnum.TV_SERIES_BORED_FILM.name()) && isBoxingLink(link)).collect(toList());
        orderLinks(list);
        List<LinksEntityResponse> newList = new ArrayList<>(list.size());
        list.forEach(e -> newList.add(mapObject((LinksEntity) e)));

        boxingLinksMap.remove(TVSeriesEnum.TV_SERIES_BORED_FILM.name());
        boxingLinksMap.put(TVSeriesEnum.TV_SERIES_BORED_FILM.name(), newList);
    }

    private static boolean isDateWithinLast30Days(LinksEntity link) {
        Date today = new Date();
        Calendar cal = new GregorianCalendar();
        cal.setTime(today);
        cal.add(Calendar.DAY_OF_MONTH, -30);
        return link.getDateAdded().after(cal.getTime());
    }

    private static boolean isMMALink(LinksEntity link) {
        return (StringUtils.equals(link.getLinkCategory(), MartialArtsEnum.MOTIVATION.name()) || !StringUtils.equals(link.getLinkCategory(),BoxingCategoriesEnum.DOCUMENTARY_BOXING_PRO.name()) && !EnumUtils.isValidEnum(BoxingCategoriesEnum.class, link.getLinkCategory()));
    }

    private static boolean isBoxingLink(LinksEntity link) {
        return EnumUtils.isValidEnum(BoxingCategoriesEnum.class, link.getLinkCategory());
    }

    public static String mapImageLink(String linkUrl, String linkName) {
        String imageLink = null;

        if (linkUrl.indexOf("youtube") >= 0 && linkName.indexOf("YouTube Channel") <= 0) {

            imageLink = "http://img.youtube.com/vi/" + StringUtils.substringAfter(linkUrl, "embed/") + "/hqdefault.jpg";


        } else if (linkUrl.indexOf("player.vimeo") >= 0) {

            imageLink = "http://thisismma-env.eba-gvxcr5y2.us-east-1.elasticbeanstalk.com/vivemo/" + StringUtils.substringAfter(linkUrl, "video/") + ".jpg";

        } else if (linkUrl.indexOf("dailymotion") >= 0) {

            imageLink = "https://www.dailymotion.com/thumbnail/video/" + StringUtils.substringAfter(linkUrl, "video/");


        } else if (linkUrl.indexOf("metacafe") >= 0) {

            // http://www.techdilate.com/code/metacafe-get-metacafe-video-thumbnails-from-url/

            imageLink = "";

        }

        return imageLink;
    }

    public static String mapVideoLink(String linkUrl) {
        String videoLink = null;
        if (linkUrl.indexOf("youtube") >= 0) {

            videoLink = "https://www.youtube.com/watch?v=" + StringUtils.substringAfter(linkUrl, "embed/");


        } else if (linkUrl.indexOf("player.vimeo") >= 0) {

            videoLink = "https://vimeo.com/" + StringUtils.substringAfter(linkUrl, "video/");

        } else if (linkUrl.indexOf("dailymotion") >= 0) {

            videoLink = "https://www.dailymotion.com/video/" + StringUtils.substringAfter(linkUrl, "video/");


        } else if (linkUrl.indexOf("metacafe") >= 0) {


            videoLink = "http://www.metacafe.com/watch/" + StringUtils.substringAfter(linkUrl, "embed/");

        }
        return videoLink;
    }


    public static String mapVideoType(String linkUrl) {
        String videoType = null;

        if (linkUrl.indexOf("youtube") >= 0) {

            videoType = "YT";


        } else if (linkUrl.indexOf("player.vimeo") >= 0) {

            videoType = "V";

        } else if (linkUrl.indexOf("dailymotion") >= 0) {

            videoType = "DM";


        } else if (linkUrl.indexOf("metacafe") >= 0) {


            videoType = "M";

        }
        return videoType;
    }


    public static String mapVideoId(String linkUrl) {
        String videoId = null;

        if (linkUrl.indexOf("youtube") >= 0) {

            videoId = StringUtils.substringAfter(linkUrl, "embed/");


        } else if (linkUrl.indexOf("player.vimeo") >= 0) {

            videoId = StringUtils.substringAfter(linkUrl, "video/");

        } else if (linkUrl.indexOf("dailymotion") >= 0) {

            videoId = StringUtils.substringAfter(linkUrl, "video/");


        } else if (linkUrl.indexOf("metacafe") >= 0) {


            videoId = StringUtils.substringAfter(linkUrl, "embed/");

        }
        return videoId;
    }


    private static Comparator<? super LinksEntity> naturalOrdering() {
        final Pattern compile = Pattern.compile("(\\d+)|(\\D+)");
        return (s1, s2) -> {
            final Matcher matcher1 = compile.matcher(s1.getLinkName());
            final Matcher matcher2 = compile.matcher(s2.getLinkName());
            while (true) {
                final boolean found1 = matcher1.find();
                final boolean found2 = matcher2.find();
                if (!found1 || !found2) {
                    return Boolean.compare(found1, found2);
                } else if (!matcher1.group().equals(matcher2.group())) {
                    if (matcher1.group(1) == null || matcher2.group(1) == null) {
                        return matcher1.group().compareTo(matcher2.group());
                    } else {
                        return Integer.valueOf(matcher1.group(1)).compareTo(Integer.valueOf(matcher2.group(1)));
                    }
                }
            }
        };
    }

    public static void orderLinks(List<LinksEntity> links) {
        links.sort(naturalOrdering());
        //orderByName(links);
        //orderByNumber(links);
    }


    private static void orderByName(List<LinksEntity> links) {

        if (links.size() > 0) {

            Collections.sort(links, Comparator.comparing(LinksEntity::getLinkName));

        }

    }

    private static void orderByNumber(List<LinksEntity> links) {

        if (links.size() > 0) {

            Collections.sort(links, (object1, object2) -> Long.signum(fixString(object1.getLinkName()) - fixString(object2.getLinkName())));

        }

    }

    private static long fixString(String s) {

        String num = s.replaceAll("\\D", "");

        // return 0 if no digits found
        return num.isEmpty() ? 0 : Long.parseLong(num);

    }
}
