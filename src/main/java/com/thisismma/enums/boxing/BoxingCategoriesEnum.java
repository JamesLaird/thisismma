package com.thisismma.enums.boxing;

/**
 * Enum to hold the type of links
 *
 * @author James Laird
 */
public enum BoxingCategoriesEnum { // 8

    VIDEO_BOXING_TRAINING("VIDEO_BOXING_TRAINING"),

    DOCUMENTARY_BOXING_PRO("DOCUMENTARY_BOXING_PRO"),
    DOCUMENTARY_BOXING("DOCUMENTARY_BOXING"),
    DOCUMENTARY_BOXING_FIGHTER("DOCUMENTARY_BOXING_FIGHTER"),
    MARTIAL_ARTS_BOXING("MARTIAL_ARTS_BOXING"),
    MOTIVATION("MOTIVATION"),
    BOXING_FIGHTERS("BOXING_FIGHTERS");


    private String linkCategory;

    BoxingCategoriesEnum(final String linkCategory) {
        this.linkCategory = linkCategory;
    }

    @Override
    public String toString() {
        return linkCategory;
    }

}
