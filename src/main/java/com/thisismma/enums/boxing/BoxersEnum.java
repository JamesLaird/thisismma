package com.thisismma.enums.boxing;

public enum BoxersEnum {

    //https://thekarateblog.com/best-boxers-of-all-time/
    // https://en.wikipedia.org/wiki/Boxing_pound_for_pound_rankings

    //add 2 women including katie

    // lomochenko, japanese, bernitove

    ALI("ALI"),
    BERNARD_HOPKINS("BERNARD_HOPKINS"),
    CANELO_ALVERAZ("CANELO_ALVERAZ"),
    EVANDER_HOLYFIELD("EVANDER_HOLYFIELD"),
    FLOYD_MAYWEATHER("FLOYD_MAYWEATHER"),
    GENNADY_GOLOVKIN("GENNADY_GOLOVKIN"),
    GERVONATA_DAVIES("GERVONATA_DAVIES"),
    GEORGE_FOREMAN("GEORGE_FOREMAN"),
    JACK_DEMPSEY("JACK_DEMPSEY"),
    JACK_JOHNSON("JACK_JOHNSON"),
    JULIO_CESAR_CHAVEZ("JULIO_CESAR_CHAVEZ"),
    JOE_LEWIS("JOE_LEWIS"),
    JOE_FRAZIER("JOE_FRAZIER"),
    JOSH_TAYLOR("JOSH_TAYLOR"),
    KATE_TAYLOR("KATE_TAYLOR"),
    LARRY_HOLMES("LARRY_HOLMES"),
    LENNOX_LEWIS("LENNOX_LEWIS"),
    MANNY_PACQUIAO("MANNY_PACQUIAO"),
    MARVIN_HAGLER("MARVIN_HAGLER"),
    MIKE_TYSON("MIKE_TYSON"),
    RICKY_HATTON("RICKY_HATTON"),
    ROBERTO_DURAN("ROBERTO_DURAN"),
    ROCKY_MARCIANO("ROCKY_MARCIANO"),
    SONNY_LISTON("SONNY_LISTON"),
    SUGAR_RAY_ROBINSON("SUGAR_RAY_ROBINSON"),
    SUGAR_RAY_LEONARD("SUGAR_RAY_LEONARD"),
    TERRANCE_CRAWFORD("TERRANCE_CRAWFORD"),
    THOMAS_HEARNS("THOMAS_HEARNS"),
    TYSON_FURY("TYSON_FURY");

    private String boxer;

    BoxersEnum(final String boxer) {
        this.boxer = boxer;
    }

    @Override
    public String toString() {
        return boxer;
    }
}
