package com.thisismma.enums.boxing;

public enum BoxingTrainingEnum {

    //boxing southpaw?
    //Ring Generalship - search ring
    //Seminars ? search on linkname
    //drills - search drills
    //seminars
    //improve your boxing tips playlist
    //skipping


    //boxing techniques
    JAB("JAB"),
    CROSS("CROSS"),
    HOOK("HOOK"),
    SHOVEL_HOOK("SHOVEL_HOOK"),
    UPPER_CUT("UPPER_CUT"),
    OVERHAND("OVERHAND"),
    COMBINATIONS("COMBINATIONS"),
    BODY_PUNCHING("BODY_PUNCHING"), 
    EVASION("EVASION"),
    DEFENCE_COUNTER("DEFENCE_COUNTER"),

    //Attributes
    STRIKING_POWER("STRIKING_POWER"), // boxing power
    STRIKING_SPEED("STRIKING_SPEED"), // boxing speed
    FOOTWORK("FOOTWORK"),

    //Equipment Training
    EQUIPMENT_TRAINING_HEAVY_BAG("EQUIPMENT_TRAINING_HEAVY_BAG"),
    EQUIPMENT_TRAINING_PADS("EQUIPMENT_TRAINING_PADS"),
    EQUIPMENT_TRAINING_SPEED_BAG("EQUIPMENT_TRAINING_SPEED_BAG"), //done - break down into heavy bag etc
    EQUIPMENT_TRAINING_DOUBLE_END_BAG("EQUIPMENT_TRAINING_DOUBLE_END_BAG"),

    //Training Techniques
    SHADOW_BOXING("SHADOW_BOXING"),
    SPARRING("SPARRING"),

    //Boxing Misc
    WRAPPING_HANDS("WRAPPING_HANDS"),
    CONDITIONING("CONDITIONING"),

    //mindset

    GENERAL("GENERAL"),

    BOXING_EQUIPMENT_REVIEWS("BOXING_EQUIPMENT_REVIEWS"); //// To Do reviews - phase 2 gloves etc


    private String technique;

    BoxingTrainingEnum(String technique) {
        this.technique = technique;
    }

    public String getTechnique() {
        return technique;
    }

    @Override
    public String toString() {
        return technique;
    }


}
