package com.thisismma.enums.mma;

public enum MMATeamsEnum { // 26 > 30

    //https://en.wikipedia.org/wiki/List_of_professional_MMA_training_camps
    // https://www.fightready.com/
    // https://en.wikipedia.org/wiki/Red_Devil_Sport_Club

    // https://www.ufc.com/news/gym-spotlight-mma-lab

    //https://www.ufc.com/news/gym-spotlight-team-alpha-male

    AKA("AKA"),
    ALL_STARS("ALL_STARS"),
    ALLIANCE_MMA("ALLIANCE_MMA"),
    AMC_PANKRATION("AMC_PANKRATION"),
    ARIZONA_COMBAT_SPORTS("ARIZONA_COMBAT_SPORTS"),
    ATT("ATT"),
    BLACKZILLIANS("BLACKZILLIANS"),
    CESAR_GRACIE("CESAR_GRACIE"),
    CITY_KICKBOXING("CITY_KICKBOXING"),
    DINKY_NINJAS("DINKY_NINJAS"),
    ELEVATION_FIGHT_TEAM("ELEVATION_FIGHT_TEAM"),
    EVOLVE_MMA("EVOLVE_MMA"),
    FACTORY_X("FACTORY_X"),
    FIGHT_READY("FIGHT_READY"),
    GRUDGE_TRAINING_CENTRE("GRUDGE_TRAINING_CENTRE"),
    HARD_KNOCKS_365("HARD_KNOCKS_365"), //Sanford MMA
    JACKSON_WINK("JACKSON_WINK"),
    KINGS_MMA("KINGS_MMA"),
    LONDON_SHOOTFIGHTERS("LONDON_SHOOTFIGHTERS"),
    MJOLNIR_MMA("MJOLNIR_MMA"),
    MMA_FACTORY_PARIS("MMA_FACTORY_PARIS"),
    MMA_LAB("MMA_LAB"),
    NOVA_UNIAO("NOVA_UNIAO"),
    ROUFUSPORT("ROUFUSPORT"),
    SBG_IRELAND("SBG_IRELAND"),
    SERRA_LONGO("SERRA_LONGO"),
    STRONG_STYLE_MMA("STRONG_STYLE_MMA"),
    SYNDICATE_MMA("SYNDICATE_MMA"),
    TEAM_ALPHA_MALE("TEAM_ALPHA_MALE"),
    TEAM_LLOYD_IRVIN("TEAM_LLOYD_IRVIN"),
    TEAM_KAOBON_MMA("TEAM_KAOBON_MMA"),
    TEAM_RENEGADE_JIU_JITSU_AND_MMA("TEAM_RENEGADE_JIU_JITSU_AND_MMA"),
    TEAM_QUEST("TEAM_QUEST"),
    TRISTAR("TRISTAR"),
    XTREME_COUTURE("XTREME_COUTURE");

    public static MMATeamsEnum fromString(String text) {
        if (text != null) {
            for (MMATeamsEnum b : MMATeamsEnum.values()) {
                if (text.equalsIgnoreCase(b.toString())) {
                    return b;
                }
            }
        }
        return null;
    }

    private String mmaTeams;

    MMATeamsEnum(final String mmaTeams) {
        this.mmaTeams = mmaTeams;
    }

    @Override
    public String toString() {
        return mmaTeams;
    }

}
