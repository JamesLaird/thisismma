package com.thisismma.enums.mma;

public enum VideoTypesEnum {

    HIGHLIGHT_VIDEOS("HIGHLIGHT_VIDEOS"),
    DOCUMENTARY_VIDEOS("DOCUMENTARY_VIDEOS"),
    PLAYLIST_VIDEOS("PLAYLIST_VIDEOS"),
    FIGHTERS_VIDEOS("FIGHTERS_VIDEOS"),
    MMA_TEAMS_VIDEOS("MMA_TEAMS_VIDEOS"),
    TV_SERIES_VIDEOS("TV_SERIES_VIDEOS"),
    FILM_PRODUCER_VIDEOS("FILM_PRODUCER_VIDEOS");

    private String video;

    VideoTypesEnum(final String video) {
        this.video = video;
    }

    @Override
    public String toString() {
        return video;
    }
}
