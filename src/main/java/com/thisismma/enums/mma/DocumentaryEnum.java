package com.thisismma.enums.mma;

/**
 * Enum to hold the type of links
 *
 * @author James Laird
 *
 */
public enum DocumentaryEnum {

    //To find > Fightville, Notorios, Such Great Heights, GSP v Crow, ufc one 20 years, buffalo girls, idris elba

    // https://www.youtube.com/results?search_query=muay+thai+documentary&sp=EgIIAw%253D%253D
    //https://www.youtube.com/watch?v=MXgHhLk2pq8&t=53s have a look please
    //https://www.youtube.com/results?search_query=Under+Pressure%22+Diary+of+a+Cagefighters+Wife - Find
    //https://www.youtube.com/watch?v=KW3GJ8KEYlg find mt

    //https://www.youtube.com/watch?v=MXgHhLk2pq8&t=344s
    //https://medium.com/martial-arts-unleashed/best-muay-thai-documentaries-e216a7991f5b
    //https://undertheropes.com/2014/07/08/muay-thai-documentaries-and-tv-shows-the-directory/
    //https://www.reddit.com/r/MuayThai/comments/a7nrmp/documentaries_to_watch/

    DOCUMENTARY_BJJ("DOCUMENTARY_BJJ"),
    DOCUMENTARY_JUDO("DOCUMENTARY_JUDO"),
    //DOCUMENTARY_KARATE("DOCUMENTARY_KARATE"),
    DOCUMENTARY_MARTIAL_ARTS("DOCUMENTARY_MARTIAL_ARTS"),
    DOCUMENTARY_MMA("DOCUMENTARY_MMA"),
    DOCUMENTARY_MMA_FIGHTER("DOCUMENTARY_MMA_FIGHTER"),
    DOCUMENTARY_MUAY_THAI("DOCUMENTARY_MUAY_THAI"),
    DOCUMENTARY_MUAY_THAI_FIGHTER("DOCUMENTARY_MUAY_THAI_FIGHTER"),
    DOCUMENTARY_WRESTLING("DOCUMENTARY_WRESTLING");

    private String documentary;

    DocumentaryEnum(final String documentary) {
        this.documentary = documentary;
    }

    @Override
    public String toString() {
        return documentary;
    }

}
