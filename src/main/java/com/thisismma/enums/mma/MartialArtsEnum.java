package com.thisismma.enums.mma;

/**
 * Enum to hold the type of links
 *
 * @author James Laird
 */
public enum MartialArtsEnum { // 8

    //to add
    //https://www.youtube.com/watch?v=7UHP9u1rIM8
    //https://www.reddit.com/r/MMA/comments/15xbh7/i_compiled_a_list_of_some_of_the_bestmost/

    MARTIAL_ARTS("MARTIAL_ARTS"),
    MARTIAL_ARTS_BJJ("MARTIAL_ARTS_BJJ"),
    //MARTIAL_ARTS_BOXING("MARTIAL_ARTS_BOXING"),
    MARTIAL_ARTS_JUDO("MARTIAL_ARTS_JUDO"),
    //MARTIAL_ARTS_KARATE("MARTIAL_ARTS_KARATE"),
    MARTIAL_ARTS_MUAY_THAI("MARTIAL_ARTS_MUAY_THAI"),
    MARTIAL_ARTS_WRESTLING("MARTIAL_ARTS_WRESTLING"),
    MARTIAL_ARTS_MMA("MARTIAL_ARTS_MMA"),
    MOTIVATION("MOTIVATION"),
    FIGHTER_HIGHLIGHTS("FIGHTER_HIGHLIGHTS"),
    MUAY_THAI_FIGHTERS("MUAY_THAI_FIGHTERS");
    //BOXING_FIGHTERS("BOXING_FIGHTERS");


    private String linkCategory;

    MartialArtsEnum(final String linkCategory) {
        this.linkCategory = linkCategory;
    }

    @Override
    public String toString() {
        return linkCategory;
    }

}
