package com.thisismma.enums.mma;

/**
 * Enum to hold the type of links 
 * 
 * @author James Laird
 * 
 */
public enum PlaylistEnum { // 14 > 15 (With JRE show x 30 MMA interviews)

    //cage-warriors, ksw
    //https://www.youtube.com/user/themmaclinic/videos
    //https://www.youtube.com/watch?v=vVTuujyBimE

     //ALI("ALI"),
	 BELLATOR("BELLATOR"),
	 BRAZILIAN_MMA("BRAZILIAN_MMA"),
     BRUCE_LEE("BRUCE_LEE"),
     CAGE_WARRIORS("CAGE_WARRIORS"),
     GRACIE("GRACIE"),
     KSW("KSW"),
     MMA_PIONEERS("MMA_PIONEERS"),
     ONE_FC("ONE_FC"),
     PRIDE("PRIDE"),
     RECOMMENDED("RECOMMENDED"),
     UFC("UFC"),
     UK_MMA("UK_MMA"),
     WOMENS_MMA("WOMENS_MMA");

	private String playlist;

	PlaylistEnum(final String playlist) {
		this.playlist = playlist;
	}

	@Override
	public String toString() {
		return playlist;
	}

}
