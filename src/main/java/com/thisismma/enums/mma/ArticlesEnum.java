package com.thisismma.enums.mma;

/**
 * Enum to hold the type of links
 *
 * @author James Laird
 */
public enum ArticlesEnum { // 8
    
    //ARTICLE_BJJ("ARTICLE_BJJ"),
    ARTICLE_BOXING("ARTICLE_BOXING");
    //ARTICLE_JUDO("ARTICLE_JUDO"),
    //ARTICLE_MUAY_THAI("ARTICLE_MUAY_THAI"),
    //ARTICLE_WRESTLING("ARTICLE_WRESTLING");

    private String linkCategory;

    ArticlesEnum(final String linkCategory) {
        this.linkCategory = linkCategory;
    }

    @Override
    public String toString() {
        return linkCategory;
    }

}