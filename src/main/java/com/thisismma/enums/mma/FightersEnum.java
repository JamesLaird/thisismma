package com.thisismma.enums.mma;

public enum FightersEnum { // 5 > 30

    //https://www.gamblingsites.org/sports-betting/mma/famous-fighters/
    //https://www.tapology.com/rankings/top-ten-fan-favorite-mma-and-ufc-fighters

    //// https://www.tapology.com/rankings/top-ten-all-time-greatest-mma-and-ufc-fighters
    //// 10 fighters plus 4 fighters + fight tracks DJ Johmson - Fighters split 4

    //highights, interviews, documentaries


    /*
    Jan Blachowicz
    chuck, dan hen, matt, tito, forest
    Petr Yan
    */

    ANDERSON_SILVA("ANDERSON_SILVA"),
    ANTHONY_PETTIS("ANTHONY_PETTIS"),
    AMANDA_NUNES("AMANDA_NUNES"),
    BJ_PENN("BJ_PENN"),
    CHARLES_OLIVEIRA("CHARLES_OLIVEIRA"),
    CONOR_MCGREGOR("CONOR_MCGREGOR"),
    DANIEL_CORMIER("DANIEL_CORMIER"),
    DEIVESON_FIGUEIREDO("DEIVESON_FIGUEIREDO"),
    DEMETRIOUS_JOHNSON("DEMETRIOUS_JOHNSON"),
    DOMINICK_CRUZ("DOMINICK_CRUZ"),
    DONALD_CERRONE("DONALD_CERRONE"),
    DUSTIN_POIRIER("DUSTIN_POIRIER"),
    FEDOR("FEDOR"),
    GSP("GSP"),
    ISRAEL_ADESANYA("ISRAEL_ADESANYA"),
    JON_JONES("JON_JONES"),
    JORGE_MASVIDAL("JORGE_MASVIDAL"),
    KHABIB_NURMAGOMEDOV("KHABIB_NURMAGOMEDOV"),
    KAMARU_USMAN("KAMARU_USMAN"),
    MICHAEL_BISPING("MICHAEL_BISPING"),
    NICK_DIAZ("NICK_DIAZ"),
    NATE_DIAZ("NATE_DIAZ"),
    RONDA_ROUSEY("RONDA_ROUSEY"),
    STIPE_MIOCIC("STIPE_MIOCIC"),
    TONY_FERGUSON("TONY_FERGUSON"),
    VALENTINA("VALENTINA"),
    ZHANG_WEILI("ZHANG_WEILI");

    private String fighters;

    FightersEnum(final String fighters) {
        this.fighters = fighters;
    }

    @Override
    public String toString() {
        return fighters;
    }

}
