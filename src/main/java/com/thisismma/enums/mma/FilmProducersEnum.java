package com.thisismma.enums.mma;

/**
 * Enum to hold the type of links 
 * 
 * @author James Laird
 * 
 */
public enum FilmProducersEnum { // 4 > 5

	//check all https://www.youtube.com/user/bobbyrazak/videos

     MACHINEMAN("MACHINEMAN"),
     RAZAK_SPORTS_FILMS("RAZAK_SPORTS_FILMS"),
     RYAN_JONES("RYAN_JONES"),
     STUART_COOPER("STUART_COOPER");

	private String playlist;

	FilmProducersEnum(final String playlist) {
		this.playlist = playlist;
	}

	@Override
	public String toString() {
		return playlist;
	}

}
