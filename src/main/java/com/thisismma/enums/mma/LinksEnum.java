package com.thisismma.enums.mma;

/**
 * Enum to hold the type of links
 *
 * @author James Laird
 */
public enum LinksEnum { // 12

    /*LINK_BJJ("LINK_BJJ"),
    LINK_BOXING("LINK_BOXING"),
    LINK_EBOOKS_OR_PDFS("LINK_EBOOKS_OR_PDFS"),
    LINK_EVENTS("LINK_EVENTS"),
    LINK_FIGHTERS("LINK_FIGHTERS"),
    LINK_JUDO("LINK_JUDO"),
    LINK_MARTIAL_ARTS_FORUMS("LINK_MARTIAL_ARTS_FORUMS"),
    LINK_MMA("LINK_MMA"),*/
    LINK_BOXING("LINK_BOXING"),
    LINK_MMA_CLUBS("LINK_MMA_CLUBS");
    /*LINK_MUAY_THAI("LINK_MUAY_THAI"),
    LINK_WRESTLING("LINK_WRESTLING"),
    LINK_YOUTUBE_CHANNELS("LINK_YOUTUBE_CHANNELS");*/

    //https://www.bjjsuccess.com/best-bjj-books/
    //podcasts links

    private String linkCategory;

    LinksEnum(final String linkCategory) {
        this.linkCategory = linkCategory;
    }

    @Override
    public String toString() {
        return linkCategory;
    }

}

