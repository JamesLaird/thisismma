package com.thisismma.error;

public class LinkErrorResponse {

    private String error;

    public  LinkErrorResponse(String error) {
        this.error = error;
    }

    public String getError() {
        return error;
    }

}
