package com.thisismma.uploadLinks;


import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import java.io.*;

public class UploadLinks {

    //Searc1
    //Searh2

    private static void loadCSV() {

        String fileName = "JREMMASHOW.htm";
        String filePath = "/Users/JLA57/Desktop/upload/"+ fileName;
        BufferedReader br = null;
        String line = "";
        String cvsSplitBy = ",";

        try {

            File input = new File(filePath.toString());
            Document doc = Jsoup.parse(input, "UTF-8", "".toString());
            Elements links = doc.select("a[href]".toString());

            for (Element link : links) {

                populateLinks(link);

            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static void populateLinks(Element link) {

        String orginalHref = link.attr("href");

        String nameOfLink = link.text();

        if ( StringUtils.contains(nameOfLink, "JRE MMA Show" ) && StringUtils.contains(orginalHref, "https://www.youtube.com/watch?v=" )) {

            System.out.println(" orginalHref  > " + orginalHref);

            System.out.println(" nameOfLink  > " + nameOfLink);
        }
    }
    public static void main(String[] args) {
        // write your code here

        //load csv
        loadCSV();
        //file rename
    }
}
