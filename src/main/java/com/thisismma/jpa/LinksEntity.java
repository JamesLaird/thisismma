package com.thisismma.jpa;

import java.util.Date;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.thisismma.mapper.LinksMapper;
import org.hibernate.annotations.GenericGenerator;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "linksData")
@Getter
@Setter
public class LinksEntity implements java.io.Serializable {

    private static final long serialVersionUID = 3139432253084317652L;

    @GenericGenerator(name = "generator", strategy = "increment")

    @Id
    @GeneratedValue(generator = "generator")
    @Column(name = "id", unique = true, nullable = false)
    private Long id;


    @Column(name = "deleted_flag", columnDefinition = "boolean default false")
    private Boolean deletedFlag;


    @Column(name = "link_category", nullable = false, length = 30)
    protected String linkCategory;

    @Column(name = "link_name", nullable = false, length = 250)
    private String linkName;

    @Column(name = "link_url", nullable = false, length = 250)
    private String linkUrl;

    @Column(name = "embed_blocked", columnDefinition = "boolean default false")
    private Boolean embedBlocked;


    @Column(name = "date_added", nullable = false)
    private Date dateAdded;


    @Column(name = "playlist_a", length = 50)
    private String playlistA;


    @Column(name = "playlist_b", length = 50)
    private String playlistB;


    @Column(name = "include_as_video_of_day", columnDefinition = "boolean default false")
    private Boolean includeAsVideoOfDay;


    @Column(name = "include_in_main_section", columnDefinition = "boolean default false")
    private Boolean includeInMainSection;

    @Transient
    private String imageLink;

    @Transient
    private Boolean doc;

    @Transient
    @JsonIgnore
    private String videoId;

    @Transient
    @JsonIgnore
    private String videoType;

    public Boolean getDoc() {
        return getLinkCategory().contains("DOCUMENTARY");
    }

    public String getImageLink() {
        return LinksMapper.mapImageLink(getLinkUrl(), getLinkName());
    }

    public String getVideoUrl() {
        return LinksMapper.mapVideoLink(getLinkUrl());
    }

    public String getVideoType() {
        return LinksMapper.mapVideoType(getLinkUrl());
    }

    public String getVideoId() {
        return LinksMapper.mapVideoId(getLinkUrl());
    }


    @Override
    public String toString() {
        return "LinksEntity{" +
                "id=" + id +
                ", deletedFlag=" + deletedFlag +
                ", linkCategory='" + linkCategory + '\'' +
                ", linkName='" + linkName + '\'' +
                ", linkUrl='" + linkUrl + '\'' +
                ", embedBlocked=" + embedBlocked +
                ", dateAdded=" + dateAdded +
                ", playlistA='" + playlistA + '\'' +
                ", playlistB='" + playlistB + '\'' +
                ", includeAsVideoOfDay=" + includeAsVideoOfDay +
                ", includeInMainSection=" + includeInMainSection +
                ", imageLink='" + imageLink + '\'' +
                '}';
    }
}