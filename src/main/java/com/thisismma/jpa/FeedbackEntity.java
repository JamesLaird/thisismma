package com.thisismma.jpa;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "user_feedback")
@Getter
@Setter
public class FeedbackEntity implements java.io.Serializable {

    private static final long serialVersionUID = 3139432253084317652L;

    @GenericGenerator(name = "generator", strategy = "increment")
    @JsonIgnore
    @Id
    @GeneratedValue(generator = "generator")
    @Column(name = "id", unique = true, nullable = false)
    private Long id;

    @Column(name = "contact_details", nullable = false, length = 250)
    private String contactDetails;

    @Column(name = "feedback", nullable = false)
    private String feedback;

    @Column(name = "date_added", nullable = false)
    private Date dateAdded;

    @Column(name = "action", length = 250)
    private String action;
}
