package com.thisismma.jpa;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class LinksEntityResponse implements java.io.Serializable {

    private static final long serialVersionUID = 3139432253084317652L;
    private String linkName;
    private String linkUrl;
    private String imageLink;
    private Boolean doc;


    public LinksEntityResponse(String linkName, String linkUrl, String imageLink, Boolean doc) {
        this.linkName = linkName;
        this.linkUrl = linkUrl;
        this.imageLink = imageLink;
        this.doc = doc;
    }
}