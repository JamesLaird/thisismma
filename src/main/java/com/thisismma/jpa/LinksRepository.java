package com.thisismma.jpa;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.List;

@RepositoryRestResource
@CrossOrigin
public interface LinksRepository extends JpaRepository<LinksEntity, Long> {

    List<LinksEntity> findByPlaylistA(String playlistA);

    List<LinksEntity> findByPlaylistB(String playlistB);

    List<LinksEntity> findByLinkCategory(String linkCategory);

    List<LinksEntity> findByEmbedBlocked(Boolean embedBlocked);

    List<LinksEntity> findByLinkUrl(String linkUrl);

}
