package com.thisismma.jpa;


import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Entity
@Table(name = "videos")
@Getter
@Setter
public class VideosEntity implements java.io.Serializable {

    private static final long serialVersionUID = 3139432253084317652L;

    @GenericGenerator(name = "generator", strategy = "increment")
    @JsonIgnore
    @Id
    @GeneratedValue(generator = "generator")
    @Column(name = "id", unique = true, nullable = false)
    private Long id;

    @Column(name = "title", nullable = false)
    private String title;

    @Column(name = "display_title")
    private String displayTitle;

    @Column(name = "playlist_type", nullable = false)
    private String playlistType;

    @Column(name = "image", nullable = false)
    private String image;

    @Column(name = "category_one")
    private String categoryOne;

    @Column(name = "category_two")
    private String categoryTwo; //Needs some work to update FE

    @Column(name = "description")
    private String description;

    @Column(name = "type")
    private String type; //Needs some work to update FE - no TYPE 2

    @Column(name = "tabs")
    private Boolean isTabs;

    @Column(name = "teams")
    private Boolean isTeams;

    @Column(name = "show_ads")
    private Boolean showAds;

}

